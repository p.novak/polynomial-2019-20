import java.util.Arrays;
import java.lang.Math;

/** Integer-only polynomials. */
public class Polynomial {

    private int coefficients [];
	
	/** Create new instance with given coefficients.
     *
     * x^2 + 3x - 7 would be created by new Polynomial(-7, 3, 1)
     * x^5 + 42 would be created by new Polynomial(42, 0, 0, 0, 0, 1)
     *
     * @param coef Coefficients, ordered from lowest degree (power).
     */
    public Polynomial(int... coef) {
        System.out.printf("Creating polynomial");
        coefficients = new int[coef.length];
        
        for (int i = 0; i < coef.length; i++) {
            coefficients[i] = coef[i];
        	if (i > 0) {
                System.out.print(" +");
            }
            System.out.printf(" %d * x^%d", coef[i], i);
        }
        System.out.println(" ...");
        checkAndFormatZero();
    }
    
    /** 
     * Checks whether the whole polynomial equals to zero. 
     * Eliminates unnecessary coefficients. 
     */
    public void checkAndFormatZero() {
        boolean isZero = true;
        for(int i = coefficients.length - 1; i >= 0; i--) {
            if (coefficients[i] != 0) {
            	isZero = false;
            	int[] newCoefficients = new int[i + 1];
            	for (int a = 0; a <= i; a++) {
            		newCoefficients[a] = coefficients[a];
            	}
            	coefficients = newCoefficients;
            	break;
            }
        }
        if (isZero) {
        	coefficients = new int[] {0};
        }
    }

    /** Get coefficient value for given degree (power). */
    public int getCoefficient(int deg) {
        if(deg <= coefficients.length - 1) {
        	return coefficients[deg];
        } else {
        	return 0;
        }
    }

    /** Get degree (max used power) of the polynomial. */
    public int getDegree() {
        if(coefficients.length == 0) {
        	return 0;
        }else{
        	return coefficients.length - 1;
        }
    }

    /** Format into human-readable form with given variable. */
    public String toPrettyString(String variable) {
        String prettyPolynomial = "";
        if(coefficients.length > 0) {
        	for(int i = coefficients.length - 1; i >= 0; i--) {
        		String nomial = "";
        		String var = "";
        		if (coefficients.length == 1 && coefficients[0] == 0) {
        			prettyPolynomial = "0";
        		}
        		
        		// formats variable into corresponding power
        		if (i > 1) {
        			var = String.format(variable + "^%d", i);
        		}
        		else if (i == 1) {
        			var = variable;
        		}
        		else if (i == 0) {
        			var = "";
        		}
        		
        		// formats nomial
        		if(coefficients[i] != 0 && Math.abs(coefficients[i]) != 1) {
        			nomial = String.format("%d%s", Math.abs(coefficients[i]), var);
        		}
        		else if(coefficients[i] == 0) {
        			continue;
        		}
        		else if(Math.abs(coefficients[i]) == 1 && i != 0) {
        			nomial = String.format("%s", var);
        		}
        		else if(Math.abs(coefficients[i]) == 1 && i == 0) {
        			nomial = "1";
        		}
        		
        		// connects nomials
        		if(prettyPolynomial.length() > 0) {
        			if (coefficients[i] < 0) {
        				prettyPolynomial = prettyPolynomial + " - " + nomial;
        			} else {
        				prettyPolynomial = prettyPolynomial + " + " + nomial;}
        		} else {
        			if (coefficients[i] < 0) {
        				prettyPolynomial = "-" + nomial;
        			}else{
        				prettyPolynomial = nomial;
        			}
        		}
        	}
        }
        return prettyPolynomial;
    }

    /** Debugging output, dump only coefficients. */
    @Override
    public String toString() {
    	String polynomialString = String.format("Polynomial%s", Arrays.toString(coefficients));
    	return polynomialString;
    }

    /** Adds together given polynomials, returning new one. */
    public static Polynomial sum(Polynomial... polynomials) {
      	// Prepares array for result's coefficients
    	int resultDegree;
    	if (polynomials[0].getDegree() >= polynomials[1].getDegree()) {
        	resultDegree = polynomials[0].getDegree();
        }else {
        	resultDegree = polynomials[1].getDegree();
        }
    	int[] resultCoef = new int[resultDegree + 1];
    	
    	// Performs operation
    	for (int i = 0; i <= resultDegree; i++) {
    		resultCoef[i] = polynomials[0].getCoefficient(i) + polynomials[1].getCoefficient(i);
    	}
    	
    	return new Polynomial(resultCoef);
    }

    /** Multiplies together given polynomials, returning new one. */
    public static Polynomial product(Polynomial... polynomials) {
    	// Prepares array for result's coefficients
    	int resultDegree = polynomials[0].getDegree() + polynomials[1].getDegree() + 1;
    	int[] resultCoef = new int[resultDegree];
    	
    	// Performs operation
    	for (int a = polynomials[0].getDegree(); a >= 0; a--) {
    		for (int b = polynomials[1].getDegree(); b >= 0; b--) {
    			resultCoef[a + b] += polynomials[0].getCoefficient(a) * polynomials[1].getCoefficient(b);
    		}
    	}
    	
    	return new Polynomial(resultCoef);
    }
    
    /** 
     * Returns result of division and remainder in following format: 
     * 	Polynomial[result, remainder] 
     * */
    private static Polynomial[] division(Polynomial dividend, Polynomial divisor) {
    	// Checks whether the division is possible or necessary
    	if (divisor.toString().contentEquals("Polynomial[0]")) {
    		throw new ArithmeticException("Cannot divide by 0");
    	}
    	if (dividend.getDegree() < divisor.getDegree()) {
    		return new Polynomial[] {new Polynomial(0), dividend};
    	}
    	
    	// Prepares array for result's coefficients and dividing nomial
    	Polynomial div = new Polynomial(dividend.coefficients);
    	int resultDegree = div.getDegree() - divisor.getDegree() + 1;
    	int[] resultCoef = new int[resultDegree];
    	int divNom = divisor.getCoefficient(divisor.getDegree());
    	
    	// Performs the division
    	for (int i = div.getDegree(); i >= divisor.getDegree(); i--) {
    		int x = div.getCoefficient(i) / divNom;
    		resultCoef[i - divisor.getDegree()] = x;
    		for (int a = 0; a <= divisor.getDegree(); a++) {
    			div.coefficients[div.getDegree() - a] -= (divisor.coefficients[divisor.getDegree() - a]) * x;
    		}
    	}
    	
    	return new Polynomial[] {new Polynomial(resultCoef), new Polynomial(div.coefficients)};
    }
    
    /** Get the result of division of two polynomials, ignoring remainder. */
    public static Polynomial div(Polynomial dividend, Polynomial divisor) {
        Polynomial[] divisionOutcome = division(dividend, divisor);
    	return divisionOutcome[0];
    }

    /** Get the remainder of division of two polynomials. */
    public static Polynomial mod(Polynomial dividend, Polynomial divisor) {
    	Polynomial[] divisionOutcome = division(dividend, divisor);
    	return divisionOutcome[1];
    }
}
