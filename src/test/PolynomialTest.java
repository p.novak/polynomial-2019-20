import static org.junit.Assert.*;
import org.junit.Test;

public class PolynomialTest {
    @Test
    public void getDegreeEmpty() {
        Polynomial a = new Polynomial();
        assertEquals(0, a.getDegree());
    }

    @Test
    public void getDegreeNumber() {
        Polynomial a = new Polynomial(42);
        assertEquals(0, a.getDegree());
    }

    @Test
    public void getDegreeCubic() {
        Polynomial a = new Polynomial(7, 8, 9, 10);
        assertEquals(3, a.getDegree());
    }

    @Test
    public void getCoefficientQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals(7, a.getCoefficient(0));
        assertEquals(8, a.getCoefficient(1));
        assertEquals(9, a.getCoefficient(2));
    }

    @Test
    public void getCoefficientOutOfRange() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals(0, a.getCoefficient(3));
        assertEquals(0, a.getCoefficient(4));
    }

    @Test
    public void toStringNumber() {
        Polynomial a = new Polynomial(42);
        assertEquals("Polynomial[42]", a.toString());
    }

    @Test
    public void toStringQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("Polynomial[7, 8, 9]", a.toString());
    }

    @Test
    public void toPrettyStringQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("9x^2 + 8x + 7", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringQuadraticWithY() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("9y^2 + 8y + 7", a.toPrettyString("y"));
    }

    @Test
    public void toPrettyStringWithOne() {
        Polynomial a = new Polynomial(1, 2, 1);
        assertEquals("x^2 + 2x + 1", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringWithZero() {
        Polynomial a = new Polynomial(3, 0, 5);
        assertEquals("5x^2 + 3", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringWithNegative() {
        Polynomial a = new Polynomial(17, 0, -2, -5);
        assertEquals("-5x^3 - 2x^2 + 17", a.toPrettyString("x"));
    }
    
    @Test
    public void toPrettyStringZero() {
        Polynomial a = new Polynomial(0, 0);
        assertEquals("0", a.toPrettyString("x"));
    }
    
    @Test
    public void sumPositive() {
    	Polynomial a = new Polynomial(2, 6, 4);
    	Polynomial b = new Polynomial(0, 4, 2, 6);
    	Polynomial c = Polynomial.sum(a, b);
    	assertEquals("Polynomial[2, 10, 6, 6]", c.toString());
    }
    
    @Test
    public void sumNegative() {
    	Polynomial a = new Polynomial(-2, -6, -4);
    	Polynomial b = new Polynomial(0, -4, -2, -6);
    	Polynomial c = Polynomial.sum(a, b);
    	assertEquals("Polynomial[-2, -10, -6, -6]", c.toString());
    }
    
    @Test
    public void sumZero() {
    	Polynomial a = new Polynomial(0);
    	Polynomial b = new Polynomial(0);
    	Polynomial c = Polynomial.sum(a, b);
    	assertEquals("Polynomial[0]", c.toString());
    }
    
    @Test
    public void productPositive() {
    	Polynomial a = new Polynomial(1, 2, 3);
    	Polynomial b = new Polynomial(0, 1, 2, 3);
    	Polynomial c = Polynomial.product(a, b);
    	assertEquals("Polynomial[0, 1, 4, 10, 12, 9]", c.toString());
    }
    
    @Test
    public void productNegative() {
    	Polynomial a = new Polynomial(1, 2, 3);
    	Polynomial b = new Polynomial(0, -1, -2, -3);
    	Polynomial c = Polynomial.product(a, b);
    	assertEquals("Polynomial[0, -1, -4, -10, -12, -9]", c.toString());
    }
    
    @Test
    public void productZero() {
    	Polynomial a = new Polynomial(0);
    	Polynomial b = new Polynomial(0, 1, 2, 3);
    	Polynomial c = Polynomial.product(a, b);
    	assertEquals("Polynomial[0]", c.toString());
    }
    
    @Test
    public void dividePositive() {
    	Polynomial a = new Polynomial(3, 2, 1);
    	Polynomial b = new Polynomial(2, 1);
    	Polynomial c = Polynomial.div(a, b);
    	assertEquals("Polynomial[0, 1]", c.toString());
    }
    
    @Test
    public void divideNegative() {
    	Polynomial a = new Polynomial(3, 2, 1);
    	Polynomial b = new Polynomial(-2, -1);
    	Polynomial c = Polynomial.div(a, b);
    	assertEquals("Polynomial[0, -1]", c.toString());
    }
    
    @Test
    public void divideZero() {
    	Polynomial a = new Polynomial(0);
    	Polynomial b = new Polynomial(0, 1, 2, 3);
    	Polynomial c = Polynomial.div(a, b);
    	assertEquals("Polynomial[0]", c.toString());
    }
    
    @Test
    public void divideShorter() {
    	Polynomial a = new Polynomial(2, 1);
    	Polynomial b = new Polynomial(3, 2, 1);
    	Polynomial c = Polynomial.div(a, b);
    	assertEquals("Polynomial[0]", c.toString());
    }
    
    @Test
    public void moduloPositive() {
    	Polynomial a = new Polynomial(3, 2, 1);
    	Polynomial b = new Polynomial(2, 1);
    	Polynomial c = Polynomial.mod(a, b);
    	assertEquals("Polynomial[3]", c.toString());
    }
    
    @Test
    public void moduloNegative() {
    	Polynomial a = new Polynomial(3, 2, 1);
    	Polynomial b = new Polynomial(-2, -1);
    	Polynomial c = Polynomial.mod(a, b);
    	assertEquals("Polynomial[3]", c.toString());
    }
    
    @Test
    public void moduloZero() {
    	Polynomial a = new Polynomial(0);
    	Polynomial b = new Polynomial(0, 1, 2, 3);
    	Polynomial c = Polynomial.mod(a, b);
    	assertEquals("Polynomial[0]", c.toString());
    }
    
    @Test
    public void moduloShorter() {
    	Polynomial a = new Polynomial(2, 1);
    	Polynomial b = new Polynomial(3, 2, 1);
    	Polynomial c = Polynomial.mod(a, b);
    	assertEquals("Polynomial[2, 1]", c.toString());
    }
}
